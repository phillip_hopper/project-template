<?php
/**
 * Created by IntelliJ IDEA.
 * User: Phil Hopper
 * Date: 26 MAR 2016
 */

if (!defined('AUTHORIZED')) die();
if (!session_id()) session_start();
if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

require_once 'exceptions.php';
require_once 'vendor/autoload.php';
require_once 'auto_loader_functions.php';


// include directories
add_subdirectory_to_path('');
add_subdirectory_to_path('classes');

// plug-ins
require_once 'config/defines.php';
require_once 'config/settings_data.php';
require_once 'include_twig.php';

// cache
global $dataCache;
$dataCache = new Cache('data');