<?php

/**
 *
 * Name: clear-cache.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: Jun 21, 2014
 *
 */
if (!defined('AUTHORIZED')) define('AUTHORIZED', 'yes');

require_once 'app_start.php';

global $dataCache;

Page::Get_Page()->Clear_Cache();
echo 'Twig cache cleared.' . PHP_EOL;

$dataCache->clear_cache();
echo 'Data cache cleared.' . PHP_EOL;
