-- *****************************************************************************
-- Script to create users table
--
-- Created 27 JAN 2016 by Phil Hopper
--
-- Default administrator password = 'adminpassword'
-- *****************************************************************************

CREATE TABLE `users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(25) NOT NULL,
  `user_password` VARCHAR(255) NOT NULL COMMENT 'can be either encoded or plain text',
  `full_name` VARCHAR(50) NOT NULL DEFAULT '',
  `user_email` VARCHAR(255) NOT NULL DEFAULT '',
  `disabled` BIT NOT NULL DEFAULT b'0',
  `created_at` TIMESTAMP NULL,
  `created_by` VARCHAR(50) NOT NULL,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_name` (`user_name`)
) ENGINE=InnoDB;

CREATE TRIGGER `tg_user_created` BEFORE INSERT ON `users` FOR EACH ROW
  SET NEW.created_at = CURRENT_TIMESTAMP();

INSERT INTO `users` (user_name, user_password, full_name, user_email, created_by, updated_by)
  VALUES ('administrator', 'adminpassword', 'System Administrator', 'admin@email.com', 'init script', 'init script');