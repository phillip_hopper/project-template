-- *****************************************************************************
-- Script to create data_settings table
--
-- Created 07 MAY 2013 by Phil Hopper
--
-- *****************************************************************************

CREATE TABLE `data_settings` (
  `session_timeout_minutes` INT NOT NULL DEFAULT 40,
  `last_expire_check` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_timeout_minutes`)
) ENGINE=MyISAM;

INSERT INTO `data_settings` (`session_timeout_minutes`) VALUES (40);
