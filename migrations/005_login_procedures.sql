-- *****************************************************************************
-- Script to create login and logout procedure
--
-- Created 31 MAR 2016 by Phil Hopper
--
-- *****************************************************************************

DELIMITER $$
CREATE PROCEDURE `sp_get_user_by_id`(IN userid INT UNSIGNED)
  BEGIN

    SELECT
      u.id,
      u.full_name,
      u.user_name,
      u.user_password,
      s.session_uuid AS __session_uuid,
      s.session_data AS __session_data
    FROM `users` AS u LEFT JOIN users_sessions AS s ON u.id = s.user_id
    WHERE u.id = userid;

  END
$$

CREATE PROCEDURE sp_remove_expired_sessions()
  BEGIN

    /* get the timeout value */
    DECLARE timeout INT;
    DECLARE last_checked TIMESTAMP;

    SELECT
      session_timeout_minutes,
      last_expire_check
    INTO timeout, last_checked
    FROM data_settings
    LIMIT 1;

    IF (timeout IS NULL)
    THEN
      SET timeout = 40;
    END IF;

    /* only do this every 5 minutes, not continuously */
    IF (last_checked IS NULL)
    THEN

      UPDATE data_settings
      SET last_expire_check = CURRENT_TIMESTAMP();

    ELSEIF (TIMESTAMPDIFF(MINUTE, last_checked, CURRENT_TIMESTAMP()) > 4)
      THEN

        /* mark expired sessions */
        UPDATE users_sessions
        SET expired = 1
        WHERE TIMESTAMPDIFF(MINUTE, last_touched, CURRENT_TIMESTAMP()) > timeout;

        /* log the action */
        INSERT INTO users_log (user_id, `action`)
          SELECT
            user_id,
            'Session Timeout'
          FROM users_sessions
          WHERE expired <> 0;

        /* delete the expired sessions */
        DELETE FROM users_sessions
        WHERE expired <> 0;

        UPDATE data_settings
        SET last_expire_check = CURRENT_TIMESTAMP();

    END IF;
  END
$$

CREATE PROCEDURE sp_user_login(IN username VARCHAR(25), IN userpwd VARCHAR(25), IN encodedpwd VARCHAR(100))
  BEGIN

    DECLARE foundid INT;
    DECLARE guid CHAR(36);

    /* remove expired sessions */
    CALL sp_remove_expired_sessions();

    /* check the user name and password */
    SELECT id
    INTO foundid
    FROM `users`
    WHERE disabled = 0 AND user_name = username
          AND (user_password = userpwd OR user_password = encodedpwd);

    /* if the user is valid, log and create new session */
    IF (foundid IS NULL)
    THEN

      /* return 0 rows to indicate failure */
      SELECT *
      FROM `users`
      WHERE id < 0
      LIMIT 1;

    ELSE

      /* remove any sessions that are still around */
      DELETE FROM users_sessions
      WHERE user_id = foundid;

      /* log the action */
      INSERT INTO users_log (user_id, `action`) VALUES (foundid, 'Log In');

      /* create new session */
      SET guid = uuid();
      INSERT INTO users_sessions (session_uuid, user_id) VALUES (guid, foundid);

      /* return the information */
      CALL sp_get_user_by_id(foundid);

    END IF;
  END
$$

CREATE PROCEDURE sp_get_user_by_session(IN guid CHAR(36))
  BEGIN

    DECLARE foundid INT UNSIGNED;

    /* remove expired sessions */
    CALL sp_remove_expired_sessions();

    /* check for existing session */
    SELECT user_id
    INTO foundid
    FROM users_sessions
    WHERE session_uuid = guid COLLATE utf8_unicode_ci;

    IF (foundid IS NULL)
    THEN

      /* return 0 rows to indicate failure */
      SELECT *
      FROM `users`
      WHERE id < 0
      LIMIT 1;

    ELSE

      /* increment the touch counter */
      UPDATE users_sessions
      SET touch_count = touch_count + 1
      WHERE user_id = foundid;

      /* return the information */
      CALL sp_get_user_by_id(foundid);

    END IF;
  END
$$

CREATE PROCEDURE sp_user_logout(IN guid CHAR(36))
  BEGIN

    DECLARE foundid INT;

    /* check the user name and password */
    SELECT user_id
    INTO foundid
    FROM users_sessions
    WHERE session_uuid = guid COLLATE utf8_unicode_ci;

    /* if the user is valid, log and create new session */
    IF (foundid IS NOT NULL)
    THEN

      /* remove any sessions that are still around */
      DELETE FROM users_sessions
      WHERE user_id = foundid;

      /* log the action */
      INSERT INTO users_log (user_id, `action`) VALUES (foundid, 'Log Out');

    END IF;

    /* remove expired sessions */
    CALL sp_remove_expired_sessions();
  END
$$
