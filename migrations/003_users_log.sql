-- *****************************************************************************
-- Script to create users_log table
--
-- Created 23 AUG 2012 by Phil Hopper
--
-- *****************************************************************************

CREATE TABLE `users_log` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `action` VARCHAR(25) NOT NULL,
  `action_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;
