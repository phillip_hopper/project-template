-- *****************************************************************************
-- Script to create users_sessions table
--
-- Created 23 AUG 2012 by Phil Hopper
--
-- *****************************************************************************

CREATE TABLE `users_sessions` (
  `session_uuid` CHAR(36) NOT NULL COMMENT 'UUID (GUID)',
  `user_id` INT UNSIGNED NOT NULL,
  `session_data` TEXT NULL,
  `touch_count` INT UNSIGNED NOT NULL DEFAULT 1,
  `last_touched` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `expired` BIT NOT NULL DEFAULT b'0',
  PRIMARY KEY (`user_id`),
  KEY `idx_uuid` (`session_uuid`)
) ENGINE=MyISAM;
