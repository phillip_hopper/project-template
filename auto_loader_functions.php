<?php
/**
 * Created by IntelliJ IDEA.
 * User: Phil Hopper
 * Date: 26 MAR 2016
 */

if (!defined('AUTHORIZED')) die();

/**
 * Adds $dirName to the end of the include path.
 * @param string $dirName
 */
function add_directory_to_path($dirName)
{
    $dirName = realpath($dirName);
    if ($dirName !== false) {
        set_include_path(get_include_path() . PATH_SEPARATOR . $dirName);
    }
}

/**
 * Adds a subdirectory, $subDirName, which is a subdirectory of this directory, to the end of the include path.
 * @param string $subDirName
 */
function add_subdirectory_to_path($subDirName)
{
    $path = dirname(__FILE__);
    if ($subDirName) {
        $path .= DS . $subDirName;
    }

    add_directory_to_path($path);
}

function file_exists_in_path($file)
{
    if (function_exists('stream_resolve_include_path')) {
        return stream_resolve_include_path($file);
    } else {
        $include_path = explode(PATH_SEPARATOR, get_include_path());
        foreach ($include_path as $path) {
            if (file_exists($path . DS . $file)) {
                return true;
            }
        }
        return false;
    }
}

function app_autoloader($class)
{
	$class = str_replace('\\', DS, $class);

    $file = $class . '.php';
    if (!file_exists_in_path($file)) {
        $file = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $class)) . '.php';

        // check for a plural version
        if (!file_exists_in_path($file)) {

            $file = $class . 's.php';
            if (!file_exists_in_path($file)) {

                $file = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $class)) . 's.php';
                if (!file_exists_in_path($file)) {

                    $file = $class . 'es.php';
                    if (!file_exists_in_path($file)) {
                        $file = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $class)) . 'es.php';
                    }
                }
            }
        }
    }

    /** @noinspection PhpIncludeInspection */
    require_once $file;
}

spl_autoload_register('app_autoloader');
