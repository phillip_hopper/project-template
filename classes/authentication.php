<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 1/30/16
 * Time: 5:33 PM
 */

use Models\AppUser;
use Models\AppUsers;

if (!defined('AUTHORIZED')) {
    die();
}

class Authentication
{
    public static $ValidatedUser = null;
    private static $cookieName = "nineteenth_hole_user";

    /**
     * Return the currently logged in user
     * @return AppUser
     */
    public static function Get_Valid_User()
    {
        /* @var $usr AppUser */
        $usr = null;

        // is this a login attempt?
        if ((isset($_POST['username'])) && (isset($_POST['password']))) {

            $usr = self::Get_User_From_Login($_POST['username'], $_POST['password']);
        } else {

            // check for logged in user
            $usr = self::Get_User_From_Cookie();
        }

        if (!$usr) {

            // if not found, reset
            self::Log_User_Out();

            // and redirect to login page
            $vars = array();
            $vars['content_template'] = 'admin_login.html.twig';
            $vars['page_title'] = 'Login';
            Page::Get_Page()->Render_Page("admin_blank.page.html.twig", $vars);

            exit();
        }

        // get user roles


        self::$ValidatedUser = $usr;

        return self::$ValidatedUser;
    }

    public static function Log_User_Out()
    {
        if (isset($_COOKIE[self::$cookieName])) {
            $session_guid = $_COOKIE[self::$cookieName];

            $db = Database::Get_Database(null); // open the default data connection

            $sql = file_get_contents(SQL_PATH . DS . 'call_user_logout.sql');
            $sql = str_replace('@session_uuid', mysqli_real_escape_string($db->cn, $session_guid), $sql);

            $db->run_sql($sql);

            Cookie::Remove_Cookie(self::$cookieName);
        }
    }


    /**
     * Checks username and password, and logs the user in
     * @param string $username
     * @param string $password
     * @return AppUser
     */
    private static function Get_User_From_Login($username, $password)
    {
        global $projectDataSettings;

        // do not try to log in if no username or password
        if ((empty($username)) || (empty($password))) {
            return null;
        }

        $db = Database::Get_Database($projectDataSettings); // open the default data connection

        $sql = file_get_contents(SQL_PATH . DS . 'call_user_login.sql');
        $sql = str_replace('@username', mysqli_real_escape_string($db->cn, $username), $sql);
        $sql = str_replace('@password', mysqli_real_escape_string($db->cn, $password), $sql);
        $sql = str_replace('@encoded_pwd', mysqli_real_escape_string($db->cn, AppUser::EncodePassword($password)),
            $sql);

        /* @var $col AppUsers */
        /* @var $user AppUser */
        $col = new AppUsers($db);
        $col->sql = $sql;
        $user = $col->get_one_object();

        if ($user) {
            if (isset($user->__session_uuid)) {
                $cookie = new Cookie(self::$cookieName, $user->__session_uuid, 0);
                $cookie->setCookie();
            }
        }

        return $user;
    }

    /**
     * Returns the logged in user
     * @return AppUser
     */
    private static function Get_User_From_Cookie()
    {
        /* @var $user AppUser */
        $user = null;

        if (isset($_COOKIE[self::$cookieName])) {

            $session_guid = $_COOKIE[self::$cookieName];

            $db = Database::Get_Database(null); // open the default data connection

            $sql = file_get_contents(SQL_PATH . DS . 'call_user_by_session.sql');
            $sql = str_replace('@session_uuid', mysqli_real_escape_string($db->cn, $session_guid), $sql);

            /* @var $col AppUsers */
            $col = new AppUsers($db);
            $col->sql = $sql;
            $user = $col->get_one_object();
        }

        return $user;
    }
}
