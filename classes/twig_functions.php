<?php

/**
 *
 * Name: twig_functions.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: 23 JAN 2016
 *
 */
class TwigFunctions {

	public static function parse_currency($value) {
		return NumberHelper::parse_currency($value);
	}

	public static function Insert_YesNo_Cell($value) {

		if ($value)
			return '<td class="yes"><i class="fa fa-check"></i></td>';  // &#x2713; green check mark
		else
			return '<td class="no"><i class="fa fa-times"></i></td>';   // &#x2717; red X
	}
}
