<?php

if (!defined('AUTHORIZED'))
	die();

/**
 *
 * Name: site_helper.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: Oct 18, 2013
 *
 */
class SiteHelper {

	private static $host;
	private static $domain;
	private static $protocol;

	private static function get_host() {

		if (empty(self::$host))
			self::$host = strtolower(Request::ServerStr('HTTP_HOST'));

		return self::$host;
	}

	private static function get_protocol() {

		if (empty(self::$protocol)) {
			self::$protocol = (Request::IsServerSet('HTTPS') && Request::ServerStr('HTTPS') !== 'off') ? 'https' : 'http';
		}

		return self::$protocol;
	}

	/**
	 * Ex. 'localhost' or 'www.google.com'
	 * @return string
	 */
	public static function Get_Request_Host() {
		return self::get_host();
	}

	/**
	 * Returns the domain name without underscores, dashes, www. or .com
	 * @return string
	 */
	public static function Get_Domain() {

		if (empty(self::$domain)) {

			// remove www
			$domain = str_replace('www.', '', self::get_host());

			// remove the suffix (.com, .net .etc)
			$pos = strrpos($domain, '.');
			if ($pos !== false)
				$domain = substr($domain, 0, $pos);

			// remove underscores and dashes
			$domain = str_replace('_', '', $domain);
			$domain = str_replace('-', '', $domain);

			self::$domain = $domain;
		}

		return self::$domain;
	}

	/**
	 * Ex. 'http://localhost' or 'http://www.google.com'
	 * @return string
	 */
	public static function Get_Protocol_And_Host() {

		return self::get_protocol() . '://' . self::Get_Request_Host();
	}

	/**
	 * Returns the URL without the query string
	 * @param string $dir Optional, the base dir for determining file name
	 * @return string
	 */
	public static function Get_FileName($dir = '') {

		$requested_file = strtok(strtolower(Request::ServerStr('REQUEST_URI')), '?');

		if (!empty($dir)) {

			$pos = strpos($requested_file, $dir);
			if ($pos !== false) {
				$tmp = substr($requested_file, $pos + strlen($dir));
				if (StringHelper::BeginsWith($tmp, '/'))
					$tmp = substr($tmp, 1);
				return $tmp;
			}
		}

		if (defined('WEB_ROOT'))
			return str_replace(WEB_ROOT . '/', '', $requested_file);

		return $requested_file;
	}

	public static function Get_Segment_After($segment) {

		$following = self::Get_FileName($segment);

		$segments = explode('/', $following);

		return $segments[0];
	}

	public static function Is_Https() {
		return self::get_protocol() == 'https';
	}

}
