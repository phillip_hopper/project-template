<?php
/**
 * Created by IntelliJ IDEA.
 * User: Phil Hopper
 * Date: 26 MAR 2016
 */

if (!defined('AUTHORIZED')) die();

class Cookie
{
    private $name;
    private $value;
    private $expire = 0;
    private $domain = '';
    private $has_changed = false;

    /**
     *
     * @param string $name
     * @param string $value
     * @param int $expire
     *
     * $expire = time() + number of seconds
     */
    public function __construct($name, $value = '', $expire = 0)
    {
        $this->name = $name;
        $this->value = $value;
        $this->expire = $expire;

        $this->has_changed = true;

        // domain must be empty for localhost
        $this->domain = self::get_domain();
    }

    public function setCookie()
    {
        if ($this->has_changed) {
            if (isset($_SERVER['SERVER_NAME'])) {
                setcookie($this->name, $this->value, $this->expire, '/', $this->domain);
            }
        }
    }

    public function deleteCookie()
    {
        if (isset($_SERVER['SERVER_NAME'])) {
            setcookie($this->name, '', time() - 86400, '/', $this->domain);
        }
    }

    private static function get_domain()
    {
        $returnval = '';

        // domain must be empty for localhost
        if (isset($_SERVER['SERVER_NAME'])) {
            $svr_name = $_SERVER['SERVER_NAME'];
            if (strtolower($svr_name) != 'localhost') {
                $parts = explode('.', $svr_name);

                // if there are 3 segments, use the root domain (2 segments)
                $part_count = count($parts);
                if ($part_count == 3) {
                    $returnval = '.' . $parts[1] . '.' . $parts[2];
                } else {
                    $returnval = '.' . $svr_name;
                }
            }
        }

        return $returnval;
    }

    public static function Remove_Cookie($cookie_name)
    {
        setcookie($cookie_name, '', time() - 86400, '/', self::get_domain());
    }

    /**
     * Returns the value of an existing cookie created on a previous page
     * @param string $cookie_name
     * @return Cookie
     */
    public static function Get_Existing_Cookie($cookie_name)
    {
        $returnval = null;

        // get an existing cookie value
        if (isset($_COOKIE[$cookie_name])) {
            $returnval = new Cookie($cookie_name);
            $returnval->value = $_COOKIE[$cookie_name];
            $returnval->has_changed = false;
        }

        return $returnval;
    }

    public function Set_Value($value)
    {
        $this->value = $value;
    }

    public function Get_Value()
    {
        return $this->value;
    }
}
