<?php
/**
 * Created by IntelliJ IDEA.
 * User: Phil Hopper
 * Date: 26 MAR 2016
 */

if (!defined('AUTHORIZED')) die();


class Cache
{
    private static $cacheDuration = 86400; // one day = 86400 seconds

    private $cacheFolder;
    private $cacheName;

    function __construct($cacheName)
    {
        $this->cacheName = $cacheName;
        $this->cacheFolder = dirname(dirname(__FILE__)) . DS . 'cache' . DS . $cacheName;

        // make sure the cache directory exists
        if (!file_exists($this->cacheFolder)) {
            mkdir($this->cacheFolder, 0775, true);
        }
    }

    function clear_cache()
    {
        MiscFunctions::clear_directory($this->cacheFolder, false);
    }

    /**
     * @param string $fileName
     * @return null|string Returns null if not found or expired
     */
    function get_cached_file($fileName)
    {
        $fileName = $this->cacheFolder . DS . $fileName;

        // if the file does not exist, return null
        if (!is_file($fileName)) {
            return null;
        }

        // check if expired
        $fileCreated = filemtime($fileName);
        if (($fileCreated + self::$cacheDuration) < time()) {
            unlink($fileName);
            return null;
        }

        return file_get_contents($fileName);
    }

    /**
     * @param string $fileName
     * @param string $contents
     */
    function put_cache_file($fileName, $contents)
    {
        $fileName = $this->cacheFolder . DS . $fileName;
        file_put_contents($fileName, $contents);
    }
}
