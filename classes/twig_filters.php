<?php
/**
 * Name: twig_filters.php
 * Description:
 *
 * Created by PhpStorm.
 *
 * Author: Phil Hopper
 * Date:   8/21/15
 * Time:   3:22 PM
 */

class TwigFilters {

	public static function decode_json($jsonString) {

		return json_decode($jsonString, true);
	}

	public static function ceil($number)
	{
		return ceil($number);
	}

	public static function currency_format($value, $decimal_places, $hide_zero = false, $hide_symbol = true) {

		if (empty($value)) {
			if ($hide_zero) return '';
			else $value = 0;
		}

		if (($value == 0) && $hide_zero) return '';

		// check for negative
		$neg = $value < 0;

		return (($neg) ? '-' : '') . ($hide_symbol ? '' : '$') . number_format(abs($value), $decimal_places);
	}

	public static function phone_format($value, $sep='.') {

		// remove non-numeric characters
		$regex = '/([^0-9])/';
		$value = preg_replace($regex, '', $value);


		if (strlen($value) > 10) $value = substr($value, strlen($value) - 10);

		if (strlen($value) == 7)
			return substr($value, 0, 3) . $sep . substr($value, 3);

		return substr($value, 0, 3) . $sep . substr($value, 3, 3) . $sep . substr($value, 6);
	}
}
