<?php

/**
 *
 * Name: request.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: Oct 19, 2013
 *
 */
class Request {

	/**
	 * Returns the $_SERVER[$variable_name] value as a string
	 * @param string $variable_name
	 * @return string
	 */
	public static function IsServerSet($variable_name) {
		return filter_input(INPUT_SERVER, $variable_name, FILTER_UNSAFE_RAW) !== null;
	}

	/**
	 * Returns the $_SERVER[$variable_name] value as a string
	 * @param string $variable_name
	 * @return string
	 */
	public static function ServerStr($variable_name) {
		return (string) filter_input(INPUT_SERVER, $variable_name, FILTER_UNSAFE_RAW);
	}

	/**
	 * Returns true if the $_POST[$variable_name] is set
	 * @param string $variable_name
	 * @return bool
	 */
	public static function IsPostSet($variable_name) {
		return filter_input(INPUT_POST, $variable_name, FILTER_UNSAFE_RAW) !== null;
	}

	/**
	 * Returns the $_POST[$variable_name] value as a string
	 * @param string $variable_name
	 * @return string
	 */
	public static function PostStr($variable_name) {
		return (string) filter_input(INPUT_POST, $variable_name, FILTER_UNSAFE_RAW);
	}

	/**
	 * Returns the $_POST[$variable_name] value as an int
	 * @param string $variable_name
	 * @return int
	 */
	public static function PostInt($variable_name) {
		return (int) filter_input(INPUT_POST, $variable_name, FILTER_SANITIZE_NUMBER_INT);
	}

	/**
	 *
	 * @param string $variable_name
	 * @param float $default
	 * @param int $decimal_places
	 * @return float
	 */
	public static function PostFloat($variable_name, $default = 0.0, $decimal_places = null) {

		$val = filter_input(INPUT_POST, $variable_name, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

		if (empty($val)) $val = $default;

		if ($decimal_places != null) $val = number_format($val, $decimal_places);

		return (float) $val;
	}

	/**
	 *
	 * @param string $variable_name
	 * @return int a timestamp on success, <b>null</b> otherwise.
	 */
	public static function PostDate($variable_name) {

		$val = self::PostStr($variable_name);

		if (empty($val)) return null;
		return strtotime($val);
	}

	/**
	 * Returns true if the $_GET[$variable_name] is set
	 * @param string $variable_name
	 * @return bool
	 */
	public static function IsGetSet($variable_name) {
		return filter_input(INPUT_GET, $variable_name, FILTER_UNSAFE_RAW) !== null;
	}

	/**
	 * Returns the $_GET[$variable_name] value as a string
	 * @param string $variable_name
	 * @return string
	 */
	public static function GetStr($variable_name) {
		return (string) filter_input(INPUT_GET, $variable_name, FILTER_UNSAFE_RAW);
	}

	/**
	 * Returns the $_GET[$variable_name] value as an int
	 * @param string $variable_name
	 * @return int
	 */
	public static function GetInt($variable_name) {
		return (int) filter_input(INPUT_GET, $variable_name, FILTER_SANITIZE_NUMBER_INT);
	}

	/**
	 * Returns the $_GET[$variable_name] value as a float
	 * @param string $variable_name
	 * @param float $default
	 * @param int $decimal_places
	 * @return float
	 */
	public static function GetFloat($variable_name, $default = 0.0, $decimal_places = null) {

		$val = filter_input(INPUT_GET, $variable_name, FILTER_SANITIZE_NUMBER_FLOAT);

		if (empty($val)) $val = $default;

		if ($decimal_places != null) $val = number_format($val, $decimal_places);

		return (float) $val;
	}

	/**
	 * Is the user a known bot?
	 * @return bool Returns true if the user is a known bot.
	 */
	public static function UserIsBot() {

		// Get the current user agent
		$userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT', FILTER_UNSAFE_RAW);

		// Check if the user agent is empty
		if (empty($userAgent)) {
			// Most browsers identify themselves, so possibly a bot
			return true;
		}

		// Declare partial bot user agents (lowercase)
		/** @noinspection SpellCheckingInspection */
		$botUserAgents = array('bot', 'spider', 'slurp', 'jeeves', 'yahoo', 'yandex', 'bing', 'msn', 'crawl', 'google');

		// Check if a bot name is in the agent
		foreach ($botUserAgents as $botUserAgent) {
			if (stripos($userAgent, $botUserAgent) !== false) {
				// If it is, return true
				return true;
			}
		}

		// check the bot_trap to determine if this is a legitimate quote request.
		// NOTE: the only valid value for 'bot_trap' is '~qazwsxedcrfvtgbyhnujmikolp+'
		if ((self::IsPostSet('bot_trap')) && (self::PostStr('bot_trap') !== '~qazwsxedcrfvtgbyhnujmikolp+'))
			return true;

		// Probably a real user
		return false;
	}
}
