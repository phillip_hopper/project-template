<?php

/**
 * Created by IntelliJ IDEA.
 * User: Phil Hopper
 * Date: 26 MAR 2016
 */

class MiscFunctions
{
    /**
     *
     * @param string $url
     * @param string $data 'name1=value1&name2=value2'
     * @return string
     */
    public static function do_post_request($url, $data)
    {
        $context_options = array(
            'http' => array(
                'method' => 'POST',
                'header' => "Content-type: application/x-www-form-urlencoded\r\n"
                    . "Content-Length: " . strlen($data) . "\r\n",
                'content' => $data
            )
        );

        $context = stream_context_create($context_options);

        $fp = @fopen($url, 'r', false, $context);  // may just need 'r' and not 'rb'

        if (!$fp) {
            //throw new Exception("Problem with $url");
            echo "Problem with $url";
            return false;
        }

        $response = stream_get_contents($fp);

        if ($response === false) {
            //throw new Exception("Problem reading data from $url");
            echo "Problem reading data from $url";
            return false;
        }

        return $response;
    }

    public static function ajax_string_to_array($str)
    {
        $rows = explode("\r\n", $str);

        if (count($rows) < 2) {
            return null;
        }

        // first row is array keys
        $keys = explode("\t", $rows[0]);
        $returnVal = array();

        // loop through remaining rows
        for ($i = 1; $i < count($rows); $i++) {

            if (strlen($rows[$i])) {
                $row = array();
                $vals = explode("\t", $rows[$i]);

                // loop through columns
                for ($j = 0; $j < count($keys); $j++) {
                    $row[$keys[$j]] = $vals[$j];
                }

                $returnVal[] = $row;
            }
        }

        return $returnVal;
    }

    /**
     *
     * @param Database $db
     * @return array
     */
    public static function recordset_to_array($db)
    {
        $returnval = array();

        $row = $db->fetch_next_object();
        while ($row) {
            $returnval[] = $row;
            $row = $db->fetch_next_object();
        }

        return $returnval;
    }

    /**
     * @param Database $db
     * @param $sqlFileName
     * @param array [$params]
     * @param string [$fileName]
     * @return array|null
     */
    public static function cachable_results_to_array($db, $sqlFileName, $params = array(), $fileName = '')
    {
        // first check the cache
        if (empty($fileName)) {
            $cacheFileName = $sqlFileName . '.cache';
        } else {
            $cacheFileName = $fileName;
        }

        $contents = self::get_from_data_cache($cacheFileName);

        // return the cached results as an array
        if ($contents !== null) {
            return $contents;
        }

        // results not cached, get from the database
        $sql = file_get_contents(SQL_PATH . DS . $sqlFileName);

        // optional parameters
        if (!empty($params)) {

            foreach ($params as $key => $value) {
                $sql = str_replace('@' . $key, $db->cn->real_escape_string($value), $sql);
            }
        }

        return self::get_sql_results_as_array_and_cache($db, $sql, $cacheFileName);
    }

    /**
     * @param Database $db
     * @param $sql
     * @param $cacheFileName
     * @return array
     */
    public static function get_sql_results_as_array_and_cache($db, $sql, $cacheFileName)
    {
        global $dataCache;

        $db->open_recordset($sql);
        $contents = self::recordset_to_array($db);

        $dataCache->put_cache_file($cacheFileName, json_encode($contents));

        return $contents;
    }

    /**
     * @param $cacheFileName
     * @return array|null
     */
    public static function get_from_data_cache($cacheFileName)
    {
        global $dataCache;

        // first check the cache
        $contents = $dataCache->get_cached_file($cacheFileName);

        // return the cached results as an array
        if ($contents !== null) {
            return json_decode($contents);
        }

        // return null if not in cache
        return null;
    }

    /**
     * Searches the include path for a file
     * @param string $file
     * @return bool
     */
    public static function file_exists_in_path($file)
    {
        $ps = explode(PATH_SEPARATOR, get_include_path());
        foreach ($ps as $path) {
            if (file_exists($path . DIRECTORY_SEPARATOR . $file)) {
                return true;
            }
        }
        if (file_exists($file)) {
            return true;
        }
        return false;
    }

    /**
     * Searches the include path for a file, returning the full path and filename if found
     * @param string $file
     * @return mixed Full path and filename if found, otherwise FALSE
     */
    public static function get_filename_in_path($file)
    {
        $ps = explode(PATH_SEPARATOR, get_include_path());
        foreach ($ps as $path) {
            $fullname = realpath($path . DIRECTORY_SEPARATOR . $file);
            if ($fullname !== false) {
                return $fullname;
            }
        }

        return false;
    }

    /**
     * Executes a php file and returns the results
     * @param string $file
     * @return string
     */
    public static function eval_php_file($file)
    {
        if (is_file($file)) {
            ob_start();
            /** @noinspection PhpIncludeInspection */
            include $file;
            return ob_get_clean();
        }
        return "";
    }

    /**
     * Removes all files and subdirectories from a directory. Optionally delete the directory also.
     * @param string $dirName
     * @param bool $deleteDirectoryAlso Delete the directory after emptying it.
     */
    public static function clear_directory($dirName, $deleteDirectoryAlso)
    {
        $structure = glob(rtrim($dirName, "/") . '/*');
        if (is_array($structure)) {
            foreach ($structure as $file) {
                if (is_dir($file)) {
                    self::clear_directory($file, true);
                } elseif (is_file($file)) {
                    unlink($file);
                }
            }
        }
        if ($deleteDirectoryAlso) {
            rmdir($dirName);
        }
    }
}
