<?php

if (!defined('AUTHORIZED')) die();

/**
 *
 * Name: number_helper.php
 * Description: Number functions
 *
 * Author: Phil Hopper
 * Created: Apr 26, 2014
 *
 */
class NumberHelper {

	public static function parse_currency($string) {

		$s = str_replace('$', '', $string);
		$s = str_replace(',', '', $s);
		$s = str_replace(' ', '', $s);
		$s = str_replace('(', '-', $s);
		$s = str_replace(')', '', $s);

		return (float) $s;
	}

	public static function format_currency($float, $decimals = 2, $include_symbol = true) {

		$is_neg = ($float < 0);

		$s = number_format($float, $decimals, '.', ',');

		if ($include_symbol) {

			$s = '$' . $s;

			if ($is_neg) {
				$s = str_replace('-', '', $s);
				$s = "-$s";
			}
		}

		return $s;
	}

	public static function Inches_to_Feet_Inches($value) {

		if (empty($value)) {
			return "N/A";
		} else {
			$inches = $value % 12;
			$feet = ($value - $inches) / 12;
			return $feet . "' " . $inches . "\"";
		}
	}

}
