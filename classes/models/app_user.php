<?php

namespace Models;

/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 1/30/16
 * Time: 12:07 PM
 */

use Database;
use DataClass;
use DataCollection;
use StringHelper;

if (!defined('AUTHORIZED')) {
    die();
}

class AppUsers extends DataCollection
{
    /**
     * AppUsers constructor.
     * @param Database $db
     */
    function __construct($db)
    {
        parent::__construct('Models\AppUser', $db);
    }
}

class AppUser extends DataClass
{
    private static $passwordPrefix = "\t64x";

    function __construct()
    {
        parent::__construct('users');
    }

    function __destruct()
    {
        parent::__destruct();
    }

    /**
     * Gets the password, decoded if it is encoded in the database
     * @return string
     */
    public function GetPassword()
    {
        $value = $this->user_password;

        if (StringHelper::BeginsWith($value, self::$passwordPrefix)) {

            // remove the prefix
            $value = substr($value, strlen(self::$passwordPrefix));

            // decode
            $value = base64_decode($value);
        }
        return $value;
    }

    /**
     * Encodes the password and sets the database value
     * @param string $value
     */
    public function SetPassword($value)
    {
        if (strlen($value) > 3) {
            $this->user_password = self::EncodePassword($value);
        }
    }

    public static function EncodePassword($value)
    {
        return self::$passwordPrefix . base64_encode($value);
    }
}