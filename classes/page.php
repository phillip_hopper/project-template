<?php

/**
 * This class renders a page using templates
 * Name: page.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: Mar 31, 2014
 *
 */
class Page
{
    // Hold an instance of the class
    private static $instance;
    private $loader;
    private $twig;

    private function __construct()
    {
        // initialize twig
        $this->loader = new Twig_Loader_Filesystem(array(
            TEMPLATE_MODULES,
            TEMPLATE_PAGES,
            TEMPLATE_CONTENT,
            TEMPLATE_CONTROLS
        ));
        $this->twig = new Twig_Environment($this->loader);

        // set up twig cache
        $cache_path = dirname(dirname(__FILE__)) . DS . 'cache' . DS . 'twig';
        $this->twig->setCache($cache_path);
        if (defined('DEBUG')) {
            $this->twig->enableAutoReload();
        } else {
            $this->twig->disableAutoReload();
        }

        // add our twig functions
        foreach (get_class_methods('TwigFunctions') as $method) {
            $this->twig->addFunction(new Twig_SimpleFunction($method, 'TwigFunctions::' . $method));
        }

        // add our twig filters
        foreach (get_class_methods('TwigFilters') as $filter) {
            $this->twig->addFilter(new Twig_SimpleFilter($filter, 'TwigFilters::' . $filter));
        }

        // add twig globals
        $this->twig->addGlobal('WEB_ROOT', WEB_ROOT);
    }

    /**
     *
     * @return Page
     */
    public static function Get_Page()
    {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    public function prependPath($path)
    {
        $this->loader->prependPath($path);
    }

    /**
     * Returns the html after putting the page together
     * @param string $template_file The file to use as the starting point
     * @param array $vars The key is the name of the variable, the value is the value of the variable.
     * @return string
     */
    public function Construct_Page($template_file, $vars = array())
    {
        $tpl = $this->twig->loadTemplate($template_file);

        $html = null;

        try {
            $html = $tpl->render($vars);

            // set web root
            $html = str_replace("~/", WEB_ROOT . "/", $html);

        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        return $html;
    }

    /**
     * Output page to the browser
     * @param string $template_file The file to use as the starting point
     * @param array $vars The key is the name of the variable, the value is the value of the variable.
     */
    public function Render_Page($template_file, $vars = array())
    {
        // clean up any stuff not part of the page
        if (ob_get_contents()) {
            ob_clean();
        }

        $html = $this->Construct_Page($template_file, $vars);

        // clean up any messes
        if (ob_get_contents()) {
            ob_clean();
        }

        echo $html;
    }

    /**
     * We needed to implement this ourselves since `clearCacheFiles()` was deprecated
     */
    public function Clear_Cache()
    {
        $cache_dir = $this->twig->getCache();
        if (is_dir($cache_dir)) {
            MiscFunctions::clear_directory($cache_dir, false);
        }
    }
}
