#!/bin/bash

thisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

lastFile="${thisDir}/.lastLessCheck"

if ! [ -f ${lastFile} ]
then
	echo "1409590000" > "$lastFile"
fi

current=`date +%s`
last=`cat ${thisDir}/.lastLessCheck`
ext=".css"
path="public/css"

# check for changes in include files
includeChanged=0
for file in ${thisDir}/less/include/*.less
do
	modified=`stat -c "%Y" ${file}`
	if [ ${modified} -gt ${last} ]
	then
		includeChanged=1
		break
	fi
done

for file in ${thisDir}/less/*.less
do
	modified=`stat -c "%Y" ${file}`
	if [ ${modified} -gt ${last} ] || [ ${includeChanged} -eq 1 ]
	then
		destination="${file/.less/$ext}"
		destination="${destination/less/$path}"

		"${thisDir}/node_modules/.bin/lessc" --include-path=${thisDir}/less/include --clean-css="--s1 -b" --no-color "$file" "$destination"
		echo "$destination"
	fi
done
echo "finished"
echo "$current" > "$lastFile"
