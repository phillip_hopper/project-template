#!/bin/bash

thisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

## Support for Less compiler
rm -f "${thisDir}/.lastLessCheck"
"${thisDir}/less.sh"

## Support for TypeScript compiler
rm -f "${thisDir}/.lastTscCheck"
"${thisDir}/tsc.sh"

## Clear cache
scriptFile="${thisDir}/clear-cache.php"
"/usr/bin/php" "${scriptFile}"

## Run migrations
scriptFile="${thisDir}/migrate.php"
"/usr/bin/php" "${scriptFile}"
