<?php
/**
 * Created by IntelliJ IDEA.
 *
 * Usage: Run `/usr/bin/php migrate.php` from a command terminal.
 *
 * User: phil
 * Date: 3/31/16
 * Time: 9:49 AM
 */

if (!defined('AUTHORIZED')) define('AUTHORIZED', 'yes');

require_once 'app_start.php';

if (empty($projectDataSettings)){
    throw new Exception('The data settings file was not found.');
}

$db = Database::Get_Database($projectDataSettings);

$command = 'mysql'
    . ' --host=' . $projectDataSettings->Server
    . ' --user=' . $projectDataSettings->User
    . ' --password=' . $projectDataSettings->Pwd
    . ' --database=' . $projectDataSettings->Database
    . ' --execute="SOURCE @file_name"';


function check_migrations_table() {
    global $db, $projectDataSettings;

    // check for the migrations table
    $sql = <<<EOD
SELECT COUNT(*)
FROM information_schema.tables
WHERE TABLE_SCHEMA = '{$projectDataSettings->Database}'
  AND TABLE_NAME = 'migrations'
EOD;

    $count = $db->execute_scalar_int($sql);

    if ($count == 0) {

        $sql = <<<EOD
CREATE TABLE `migrations` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration_name` VARCHAR(255) NOT NULL,
  `applied_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM;
EOD;

        $db->run_sql($sql);
    }
}

function migration_already_run($migration_name) {
    global $db;

    $sql = <<<EOD
SELECT COUNT(*) FROM migrations
WHERE migration_name = '{$migration_name}';
EOD;

    $count = $db->execute_scalar_int($sql);

    if ($count > 0) {
        echo $migration_name . ' already completed.' . PHP_EOL;
        return true;
    }

    return false;
}

function migrate_now($file_name, $migration_name) {
    global $db, $command;

    echo "Starting $migration_name... ";

    $cmd = str_replace('@file_name', $file_name, $command);
    $output = array();
    $exit_code = 0;
    exec($cmd, $output, $exit_code);

    // make sure no errors were reported
    if (!empty($output)) {
        echo PHP_EOL;
        var_dump($output);
        exit();
    }

    $sql = "INSERT INTO migrations (migration_name) VALUES ('$migration_name')";
    $db->run_sql($sql);

    echo 'done.' . PHP_EOL;
}

echo PHP_EOL . 'BEGINNING MIGRATION.' . PHP_EOL . PHP_EOL;

check_migrations_table();

// loop through the migration files in alphabetic order
$pattern = dirname(__FILE__) . DS . 'migrations' . DS . '*.sql';

foreach(glob($pattern) as $file_name) {

    $migration_name = basename($file_name);

    if (!migration_already_run($migration_name)) {
        migrate_now($file_name, $migration_name);
    }
}

echo PHP_EOL . 'FINISHED MIGRATION.' . PHP_EOL . PHP_EOL;
