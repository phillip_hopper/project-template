# Generic instructions for using this template


1. Open IntelliJ IDEA.
2. Select File > New > Project.
3. Select PHP from the list on the left.
4. Select 'Composer project' and click 'Next'.
5. Under 'Package' select 'sibertec/project-template'.
