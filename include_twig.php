<?php

if (!defined('AUTHORIZED')) die();

/**
 *
 * Loads Twig template engine
 *
 * Created Oct 18, 2013 by Phil Hopper
 *
 */

$dir = dirname(__FILE__) . DS . 'templates';

// TEMPLATE_MODULES is where shared headers, footers, etc are located
if (!defined('TEMPLATE_MODULES')) {
	$path = $dir . DS . 'modules';
	define('TEMPLATE_MODULES', realpath($path));
}

// TEMPLATE_CONTENT is where page content located
if (!defined('TEMPLATE_CONTENT')) {
	$path = $dir . DS . 'content';
	define('TEMPLATE_CONTENT', realpath($path));
}

// TEMPLATE_CONTROLS is where web controls are located
if (!defined('TEMPLATE_CONTROLS')) {
	$path = $dir . DS . 'controls';
	define('TEMPLATE_CONTROLS', realpath($path));
}

// TEMPLATE_PAGES is where page layout files are located
if (!defined('TEMPLATE_PAGES')) {
	$path = $dir . DS . 'pages';
	define('TEMPLATE_PAGES', realpath($path));
}
