<?php

if (!defined('AUTHORIZED')) die();

/**
 *
 * Name: settings-data.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: 23 JAN 2016
 *
 */

// TODO: change 'template-settings.json' to the name of your settings file
$dataSettingsFile = dirname(dirname(dirname(__FILE__))) . '/.settings/template-settings.json';
$projectDataSettings = is_file($dataSettingsFile) ? new DataSettings($dataSettingsFile) : null;

// the sql directory
if (!defined('SQL_PATH')) {
    $path = dirname(dirname(__FILE__));
    $path .= DS . 'sql';
    $path = realpath($path);
    if ($path != false) {
        define('SQL_PATH', $path);
    }
}
