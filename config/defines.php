<?php

/**
 *
 * Name: defines.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: Jun 7, 2014
 *
 */
// WEB_ROOT - passed to twig as a global.
// Also replaces "~/".
// Must NOT end with a forward slash ( '/' ),
// If not empty, must start with a forward slash ( '/' ),
// Ex: {{ WEB_ROOT }}/css/styles.css
// Ex: ~/css/styles.css

// TODO: change 'sub-directory' to the location of your development project
if (!defined('WEB_ROOT')) {
    if (StringHelper::Contains(SiteHelper::Get_Domain(), 'localhost')) {
        define('WEB_ROOT', '/sub-directory');
        define('DEBUG', true);
    } else {
        define('WEB_ROOT', '');
    }
}
